import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    id("org.jetbrains.kotlin.jvm").version("1.3.72")

    // Apply the application plugin to add support for building a CLI application.
    application
    jacoco
}

application {
    mainClassName = "sae.server.Server"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.http4k:http4k-core:3.254.0")
    implementation("org.http4k:http4k-server-jetty:3.254.0")
    implementation("org.http4k:http4k-client-okhttp:3.254.0")


    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.0.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")

    // A fluent assertions library.
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.22")
}

tasks.test {
    useJUnitPlatform()
}
tasks.jacocoTestReport {
    dependsOn(tasks.test)
}

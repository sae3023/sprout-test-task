Throughout this writeup and in the code I'm going to use "Boolean expression" and "Numeric expression".

When I say "Boolean expressions", I mean the substitution expressions that take the A, B and C parameters and produce an 
H output. 

When I say "Numeric expressions", I mean the substitution expressions that take the H, D, E and F and produce the K output.

## On language choice

I've recently picked up Kotlin and it's been a joy to program in, so I've decided to use it.

I don't think the language really matters for this application: as long as it's strongly typed, implementation should not get 
too hard.

Kotlin [typealiases](https://kotlinlang.org/docs/reference/type-aliases.html) simplified abstracting away the substitution expressions.

As the logic is similar between the boolean expressions and the numeric expressions, I've only needed to implement it once, 
and typealias the boolean and numeric expressions to specify the type parameters.

As a side-note: I initally wanted to parse the expressions from the request, but decided against it, as it'd take too much time.

For such a task, I think Clojure would be a perfect choice - building data structures at runtime from flaky data
seems like a good fit for a homoiconic language. But alas, Kotlin is used. 

### On `null`s

Throughout the project, `null`s are used to represent the absence of a value. `Null`s in Kotlin differ from, for example, `null`s in Java, as
in Kotlin, a type `T` and a nullable type `T?` are two distinct types: `T` values do not allow `null`s. If a method accepts a `T` parameter, it is
ensured an compile-time that a nullable reference is not passed to it. To turn `T?` into `T` you need to either explicitly assert it with `value!!`, 
signaling a potential `NullPointerException`, or check it.

In summary, `null`s in Kotlin are safe and are similar to `Option`s from other languages, albeit without the nice utility methods such as `map`.

## On library choices

This project has 3 dependencies: JUnit, a fluent assertions library and an HTTP controller library.

I use JUnit as the testing library, I don't think there's a way around that.
A fluent assertions library allows to write a more DSL-y looking `assertThat(value).isEqualTo(otherValue)` expressions, 
which I'm used to writing on my job. I think it looks nicer, and the dependency is only used in tests anyway.

Lastly, I use http4k to implement a server. I don't like big dependencies like Spring, and I've found this library to be small
and allow to expose an HTTP controller is a minimal amount of lines. It doesn't need the entire Spring runtime and doesn't
impact the codebase the same way Spring would do.

## On architecture

The project has 2 packages: `model` and `server`.

### Model

The model package contains a description of the domain: the substitution expressions and the values that they expect.

The logic is tested in `BooleanSubstitutionExpressionsTest` and `NumericSubstitutionExpressionsTest`.
The model is agnostic of how it is used, and the main piece of logic is contained within the `SubstitutionExpressions.kt` file.
It can be easily extended with new types of substitution expressions. To do so, one needs to provide the input, the output
and typealias the base and the custom substitution expressions. 

#### Base vs Custom

Base expressions and custom expressions look very similar, but they are implemented as two distinct types. This ensures
more compile-time safety and reminds the developer of the fact that they are, in fact, distinct concepts: custom expressions
override the base ones.

### Server

The `server` package contains the application main class - `Server`, which runs, well, the server.
It also contains wrapper `Request` and `Response` types, which define domain-specific requests and responses.

#### Request

Request contains all of the values needed to evaluate the outputs. This is in contrast with an HTTP request, which may not contain it.
As long as the application operates in terms of `sae.server.Request`s, the purity and the evaluation guarantees can be ensured
more strictly.

#### Response

Response is a `sealed` class - a Kotlin way to implement an algebraic data type.
It covers all of the possible scenarios:

- HTTP request did not contain enough data - `NotEnoughData` 
- The specified A, B, C did not evaluate to `true` for any substitution expressions - `BooleanEvaluationFailure`
- The evaluated H did not match any of the predefined H in the numeric expression, or a division by zero occurred - `NumericEvaluationFailure`
- The results were evaluated successfully - `Sucess`

If the `Request` object could not be constructed, the failing response is returned immediately.

## Running

Running the following command:

```sh
./gradlew run
```

launches a local Jetty server on port "4567".

To use it, specify the values using the query parameters, e.g.

`curl localhost:4567/evaluate?a=true&b=true&c=false&d=1&e=2&f=3`

Running `./gradlew test` runs the unit tests.
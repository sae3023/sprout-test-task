package sae.model

import sae.model.BooleanFunctionOutput.*
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Boolean substitution expressions should")
class BooleanSubstitutionExpressionsTest {

    companion object {
        private val base = BaseBooleanExpression(
            listOf<ThreeBooleanFunction>(
                { if (it.a && it.b && !it.c) M else null },
                { if (it.a && it.b && it.c) P else null },
                { if (!it.a && it.b && it.c) T else null })
        )

        private val custom = CustomBooleanExpression(
            listOf<ThreeBooleanFunction>(
                { if (it.a && it.b && !it.c) T else null },
                { if (it.a && !it.b && it.c) M else null }
            )
        )

        private val emptyBase = BaseBooleanExpression(listOf())
        private val emptyCustom = CustomBooleanExpression(listOf())
    }

    @Test
    @DisplayName("find a result among the base cases")
    fun findAmongBase() {
        val result = BooleanExpressions(
            base,
            emptyCustom,
            emptyCustom
        )
            .eval(ThreeBooleans(a = true, b = true, c = true))
        assertThat(result).isEqualTo(P)
    }

    @Test
    @DisplayName("return `null` if no value among base was found and there are no custom expressions")
    fun nullIfBaseDoesntMatch() {
        val result = BooleanExpressions(
            base,
            emptyCustom,
            emptyCustom
        )
            .eval(ThreeBooleans(a = false, b = false, c = false))
        assertThat(result).isNull()
    }

    @Test
    @DisplayName("find a result among the custom cases")
    fun findAmongCustom() {
        val result = BooleanExpressions(
            emptyBase,
            emptyCustom,
            custom
        )
            .eval(ThreeBooleans(a = true, b = true, c = false))
        assertThat(result).isEqualTo(T)
    }

    @Test
    @DisplayName("return `null` if not value among custom was found and there are no base expressions")
    fun nullIfCustomDoesntMatch() {
        val result = BooleanExpressions(
            emptyBase,
            emptyCustom,
            custom
        )
            .eval(ThreeBooleans(a = false, b = false, c = false))
        assertThat(result).isNull()
    }

    @Test
    @DisplayName("favour custom over base")
    fun favorCustom() {
        // Note that these booleans match a `A && B && !C` expression, which both base and custom contain.
        val result = BooleanExpressions(
            base,
            emptyCustom,
            custom
        )
            .eval(ThreeBooleans(a = true, b = true, c = false))
        assertThat(result).isEqualTo(T)
    }
}
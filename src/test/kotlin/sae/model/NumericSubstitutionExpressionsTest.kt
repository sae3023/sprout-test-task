package sae.model

import sae.model.BooleanFunctionOutput.*
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Numeric substitution expressions should")
class NumericSubstitutionExpressionsTest {

    companion object {
        private val base = BaseNumericExpressions(
            listOf(
                concreteHOrNull(M) { (it.d + (it.d * it.e / 10)) },
                concreteHOrNull(P) { (it.d + (it.d * (it.e - it.f) / 25.5)).toFloat() },
                concreteHOrNull(T) { (it.d - (it.d * it.f / 30)) }
            )
        )

        private val firstCustom = CustomNumericExpressions(
            listOf(
                concreteHOrNull(P) { (2 * it.d + (it.d * it.e / 100)) }
            )
        )

        private val emptyBase = BaseNumericExpressions(listOf())
        private val emptyCustom = CustomNumericExpressions(listOf())
    }

    @Test
    @DisplayName("find a result among the base cases")
    fun findAmongBase() {
        val d = 15f
        val e = 17
        val f = 21
        val result = NumericExpressions(
            base,
            emptyCustom,
            emptyCustom
        )
            .eval(NumericInputs(M, d, e, f))
        assertThat(result).isEqualTo((d + (d * e / 10)))
    }

    @Test
    @DisplayName("return `null` if no value among base was found and there are no custom expressions")
    fun nullIfBaseDoesntMatch() {
        val expectedH = T
        val actualH = M
        val base = BaseNumericExpressions(
            listOf(concreteHOrNull(expectedH) { (it.d + it.f + it.e) })
        )
        val result = NumericExpressions(base,
            emptyCustom,
            emptyCustom
        )
            .eval(NumericInputs(actualH, 12f, 15, 17))
        assertThat(result).isNull()
    }

    @Test
    @DisplayName("find a result among the custom cases")
    fun findAmongCustom() {
        val d = 12f
        val e = 15
        val f = 0
        val result = NumericExpressions(
            base,
            firstCustom,
            emptyCustom
        )
            .eval(NumericInputs(P, d, e, f))
        val expected = 2 * d + (d * e / 100)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    @DisplayName("return `null` if not value among custom was found and there are no base expressions")
    fun nullIfCustomDoesntMatch() {
        val actualH = M

        val result = NumericExpressions(
            emptyBase,
            firstCustom,
            emptyCustom
        )
            .eval(NumericInputs(actualH, 12f, 13, 14))
        assertThat(result).isNull()
    }

    @Test
    @DisplayName("favor custom over base")
    fun favorCustom() {
        val d = 15f
        val e = 16
        val f = 17

        // Notice that custom overrides base when it comes to P
        val result = NumericExpressions(
            base,
            firstCustom,
            emptyCustom
        )
            .eval(NumericInputs(P, d, e, f))
        val expected = 2 * d + (d * e / 100)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    @DisplayName("favor second custom over the first custom")
    fun favorSecondCustom() {
        val d = 18f
        val e = 19
        val f = 20

        val overridingCustom = CustomNumericExpressions(listOf(
            concreteHOrNull(P) { it.d + it.f + it.e }
        ))

        // Now, P is overridden twice.
        val result = NumericExpressions(
            base,
            firstCustom, overridingCustom)
            .eval(NumericInputs(P, d, e, f))
        assertThat(result).isEqualTo(d + e + f)
    }
}
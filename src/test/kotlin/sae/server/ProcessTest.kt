package sae.server

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isIn
import assertk.assertions.isInstanceOf
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import sae.model.*
import sae.model.BooleanFunctionOutput.*

@DisplayName("`process` function should")
class ProcessTest {

    companion object {
        private val baseBoolean = BaseBooleanExpression(
            listOf<ThreeBooleanFunction>(
                { if (it.a && it.b && !it.c) M else null },
                { if (it.a && it.b && it.c) P else null },
                { if (!it.a && it.b && it.c) T else null })
        )

        private val baseNumeric = BaseNumericExpressions(
            listOf(
                concreteHOrNull(M) { (it.d + (it.d * it.e / 10)) },
                concreteHOrNull(P) { (it.d + (it.d * (it.e - it.f) / 25.5)).toFloat() },
                concreteHOrNull(T) { (it.d - (it.d * it.f / 30)) }
            )
        )

        const val d = 15f
        const val e = 16
        const val f = 17

        val emptyCustomNumeric = CustomNumericExpressions(listOf())
        val emptyCustomBase = CustomBooleanExpression(listOf())
    }

    @Test
    @DisplayName("return a successful result")
    fun ok() {
        val booleanExpr = BooleanExpressions(
            baseBoolean,
            emptyCustomBase,
            emptyCustomBase
        )

        val numericExpr = NumericExpressions(
            baseNumeric,
            emptyCustomNumeric,
            emptyCustomNumeric
        )

        val request = Request(a = true, b = true, c = false, d = d, e = e, f = f)
        val response = process(request, booleanExpr, numericExpr)

        assertThat(response).isInstanceOf(Success::class)
        val h = (response as Success).h
        val k = response.k
        assertThat(h).isEqualTo(M)
        val expectedK = (d + (d * e / 10))
        assertThat(k).isEqualTo(expectedK)
    }

    @Test
    @DisplayName("return a boolean expression evaluation failure")
    fun booleanFailure() {
        val booleanExpr = BooleanExpressions(
            baseBoolean,
            emptyCustomBase,
            emptyCustomBase
        )

        val numericExpr = NumericExpressions(
            baseNumeric,
            emptyCustomNumeric,
            emptyCustomNumeric
        )

        val request = Request(a = false, b = false, c = false, d = d, e = e, f = f)
        val response = process(request, booleanExpr, numericExpr)
        assertThat(response).isInstanceOf(BooleanEvaluationFailure::class)
        val inputs = (response as BooleanEvaluationFailure).booleanInputs
        assertThat(inputs).isEqualTo(ThreeBooleans(a = false, b = false, c = false))
    }

    @Test
    @DisplayName("return a numeric expression evaluation failure")
    fun numericFailure() {
        val newNumeric = BaseNumericExpressions(
            listOf(
                concreteHOrNull(T) { it.d + it.f + it.e }
            )
        )

        val numeric = NumericExpressions(
            newNumeric,
            emptyCustomNumeric,
            emptyCustomNumeric
        )

        val boolean = BooleanExpressions(
            baseBoolean,
            emptyCustomBase,
            emptyCustomBase
        )

        val request = Request(a = true, b = true, c = false, d = d, e = e, f = f)
        val response = process(request, boolean, numeric)
        assertThat(response).isInstanceOf(NumericEvaluationFailure::class)
        val inputs = (response as NumericEvaluationFailure).numericInputs
        assertThat(inputs).isEqualTo(NumericInputs(h = M, d = d, e = e, f = f))
    }
    
    @Test
    @DisplayName("return an appropriate error for a divide-by-zero expression")
    fun divideByZero() {
        val newNumeric = BaseNumericExpressions(
            listOf(
                concreteHOrNull(M) { it.d / it.e}
            )
        )

        val boolean = BooleanExpressions(
            baseBoolean,
            emptyCustomBase,
            emptyCustomBase
        )

        val numeric = NumericExpressions(
            newNumeric,
            emptyCustomNumeric,
            emptyCustomNumeric
        )

        val request = Request(a = true, b = true, c = false, d = 1f, e = 0, f = 0)
        val response = process(request, boolean, numeric)
        assertThat(response).isInstanceOf(NumericEvaluationFailure::class)
    }
}
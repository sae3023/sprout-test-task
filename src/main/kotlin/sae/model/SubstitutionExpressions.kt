package sae.model

/**
 * A function that takes a tuple of inputs and produces a nullable output.
 */
typealias NArgumentFunction<I, R> = (I) -> R?

/**
 * An expression for substituting the [inputs][I] for the [outputs][R]. If, for a given input, one of these functions
 * returns a non-null value, the result becomes the output.
 *
 * These expressions are overridden by [CustomSubstitutionExpressions]
 */
data class BaseSubstitutionExpressions<I, R>(val functions: List<NArgumentFunction<I, R>>)

/**
 * An expression for substituting the [inputs][I] for the [outputs][R]. If, for a given input, one of these functions
 * returns a non-null value, the result becomes the output.
 *
 * These expressions override the [BaseNumericExpressions].
 */
data class CustomSubstitutionExpressions<I, R>(val functions: List<NArgumentFunction<I, R>>)

/**
 * A number of base and custom substitution expression for [evaluating][SubstitutionExpressions.eval] the result [R]
 * for a given input [I].
 */
class SubstitutionExpressions<I, R>(
    private val base: BaseSubstitutionExpressions<I, R>,
    private val firstCustom: CustomSubstitutionExpressions<I, R>,
    private val secondCustom: CustomSubstitutionExpressions<I, R>
) {

    /**
     * Evaluates the output for the given input.
     *
     * Fist, goes through the custom expressions, as they override the base ones.
     *
     * If one of the custom expressions evaluates to a non-null result for a given [I], the result is returned
     * as the output.
     *
     * If all of the custom expressions evaluate to `null`, the base expressions are checked.
     *
     * If all of the base expressions are also `null`, `null` is returned. Else, the first non-null base output
     * is returned.
     */
    fun eval(args: I): R? {
        val fromCustom = firstResult(secondCustom.functions + firstCustom.functions, args)
        if (fromCustom != null) {
            return fromCustom
        }
        return firstResult(base.functions, args)
    }

    private fun firstResult(funcs: List<NArgumentFunction<I, R>>, args: I): R? =
        funcs.map { it(args) }.firstOrNull { it != null }
}
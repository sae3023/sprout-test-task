package sae.model

/**
 * A numeric function substitutes the [BooleanFunctionOutput] and three numeric values for a numeric value, or a `null`,
 * if the [BooleanFunctionOutput] did not match an expected one.
 */
typealias NumericFunction = NArgumentFunction<NumericInputs, Float>

/**
 * An input to the numeric function is a single [BooleanFunctionOutput] and three numbers. If the
 * [BooleanFunctionOutput] has the expected value, the expression is evaluated using the numbers.
 */
data class NumericInputs(val h: BooleanFunctionOutput, val d: Float, val e: Int, val f: Int) {

    override fun toString(): String {
        return """{"d": $d, "e": $e, "f": $f}"""
    }
}

/**
 * A utility function for constructing [numeric functions][NumericFunction].
 *
 * See the tests for an example.
 */
fun concreteHOrNull(actualH: BooleanFunctionOutput, fn: NumericFunction): NumericFunction = { actualInput ->
    if (actualH == actualInput.h) {
        fn(actualInput)
    } else {
        null
    }
}

/**
 * Base expressions for substituting the numeric inputs for the outputs.
 *
 * Base expressions are overridden by [custom ones][CustomNumericExpressions].
 */
typealias BaseNumericExpressions = BaseSubstitutionExpressions<NumericInputs, Float>

/**
 * Custom expressions for substitution the numeric inputs for the outputs.
 *
 * Custom expressions override the [base ones][BaseNumericExpressions]
 */
typealias CustomNumericExpressions = CustomSubstitutionExpressions<NumericInputs, Float>

/**
 * A number of [base][BaseNumericExpressions] and [custom][CustomNumericExpressions] expressions that can
 * substitute the [numeric inputs][NumericInputs] for a `Float` value.
 */
typealias NumericExpressions = SubstitutionExpressions<NumericInputs, Float>

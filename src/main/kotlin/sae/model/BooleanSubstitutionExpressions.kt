package sae.model

/**
 * An expression over 3 boolean values that outputs one value from [BooleanFunctionOutput] if the expression is
 * evaluated to `true`, or `null` if the expression is `false`.
 */
typealias ThreeBooleanFunction = NArgumentFunction<ThreeBooleans, BooleanFunctionOutput>

/**
 * An input tuple for the boolean function.
 */
data class ThreeBooleans(val a: Boolean, val b: Boolean, val c: Boolean) {

    override fun toString(): String {
        return """{"a": $a, "b": $b, "c": $c}"""
    }
}

/**
 * Possible outputs of the boolean function.
 */
enum class BooleanFunctionOutput {
    M, P, T;

    override fun toString(): String = this.name
}

/**
 * Base expressions for substituting the inputs for the outputs.
 *
 * Base expressions are overridden by [custom ones][CustomBooleanExpression].
 */
typealias BaseBooleanExpression = BaseSubstitutionExpressions<ThreeBooleans, BooleanFunctionOutput>

/**
 * Custom expressions for substitution the inputs for the outputs.
 *
 * Custom expressions override the [base ones][BaseBooleanExpression]
 */
typealias CustomBooleanExpression = CustomSubstitutionExpressions<ThreeBooleans, BooleanFunctionOutput>

/**
 * A number of [base][BaseBooleanExpression] and [custom][CustomBooleanExpression] expressions that can
 * substitute the [boolean inputs][ThreeBooleans] for the [output][BooleanFunctionOutput].
 */
typealias BooleanExpressions = SubstitutionExpressions<ThreeBooleans, BooleanFunctionOutput>

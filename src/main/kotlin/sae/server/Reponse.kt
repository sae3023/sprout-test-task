package sae.server

import org.http4k.core.Status
import org.http4k.core.Status.Companion.BAD_REQUEST
import org.http4k.core.Status.Companion.OK
import sae.model.BooleanFunctionOutput
import sae.model.NumericInputs
import sae.model.ThreeBooleans

/**
 * A response.
 *
 * Can be either a [Success] or one of three failures.
 *
 * A [NotEnoughData] indicates that there was not enough data to proceed with the evaluation.
 *
 * A [BooleanEvaluationFailure] indicates a situation when the given boolean inputs did not evaluate to `true` according
 * to the [sae.model.BooleanExpressions].
 *
 * A [NumericEvaluationFailure] indicates a situation when the [sae.model.BooleanExpressions] evaluated to the value
 * that did not match any outputs in the [sae.model.NumericExpressions].
 */
sealed class Response(val status: Status) {

    abstract override fun toString(): String
}

object NotEnoughData : Response(BAD_REQUEST) {
    override fun toString(): String =
        """The request did not contain enough data. Please make sure to specify `a`, `b`, `c`, `d`, `e`, `f` as query strings."""

}

data class Success(val h: BooleanFunctionOutput, val k: Float) : Response(OK) {

    override fun toString(): String = """{"h": $h, "k": $k}"""
}

class BooleanEvaluationFailure(val booleanInputs: ThreeBooleans) : Response(BAD_REQUEST) {

    override fun toString(): String = """{"message": "Could not evaluate the booleans into `H`: `$booleanInputs`"}"""
}

class NumericEvaluationFailure(val numericInputs: NumericInputs) : Response(BAD_REQUEST) {

    override fun toString(): String =
        """{"message": "Could not evaluate the numeric results into `K`: `$numericInputs`"}. Beware of division by zero."""
}
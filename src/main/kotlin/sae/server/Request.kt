package sae.server

import sae.model.BooleanExpressions
import sae.model.NumericExpressions
import sae.model.NumericInputs
import sae.model.ThreeBooleans
import java.lang.RuntimeException

data class Request(val a: Boolean, val b: Boolean, val c: Boolean, val d: Float, val e: Int, val f: Int)

fun process(
    request: Request,
    booleanExpressions: BooleanExpressions,
    numericExpressions: NumericExpressions
): Response {
    val booleanInputs = ThreeBooleans(request.a, request.b, request.c)
    val boolResult = booleanExpressions.eval(booleanInputs) ?: return BooleanEvaluationFailure(booleanInputs)
    val numericInputs = NumericInputs(boolResult, request.d, request.e, request.f)
    val numericResult = numericExpressions.eval(numericInputs)

    return if (numericResult == null || !numericResult.isFinite()) {
        NumericEvaluationFailure(numericInputs)
    } else {
        Success(boolResult, numericResult)
    }
}
// Without it, Kotlin may mangle the class name.
@file:JvmName("Server")

package sae.server

import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import sae.model.*

fun main() {
    val app: HttpHandler = routes(
        "/evaluate" bind GET to { req: Request ->
            val request = mapRequest(req)
            if (request == null) {
                val resp = NotEnoughData
                Response(resp.status).body(resp.toString())
            } else {
                val resp = process(request, booleanExpressions, numericExpressions)
                Response(resp.status).body(resp.toString())
            }
        }
    )
    app.asServer(Jetty(4567)).start()
}

/**
 * Maps an HTTP request to an application request.
 */
fun mapRequest(r: Request): sae.server.Request? {
    val a = r.query("a")?.toBoolean()
    val b = r.query("b")?.toBoolean()
    val c = r.query("c")?.toBoolean()
    val d = r.query("d")?.toFloat()
    val e = r.query("e")?.toInt()
    val f = r.query("f")?.toInt()

    val atLeastOneNull = listOf(a, b, c, d, e, f).any { it == null }
    return if (atLeastOneNull) {
        null
    } else {
        // !! Is safe, since if any is null it was handled above.
        Request(a!!, b!!, c!!, d!!, e!!, f!!)
    }
}

private val booleanExpressions = BooleanExpressions(
    BaseBooleanExpression(
        listOf<ThreeBooleanFunction>(
            { if (it.a && it.b && !it.c) BooleanFunctionOutput.M else null },
            { if (it.a && it.b && it.c) BooleanFunctionOutput.P else null },
            { if (!it.a && it.b && it.c) BooleanFunctionOutput.T else null })
    ),
    CustomBooleanExpression(
        listOf<ThreeBooleanFunction>(
            { if (it.a && it.b && !it.c) BooleanFunctionOutput.T else null },
            { if (it.a && !it.b && it.c) BooleanFunctionOutput.M else null }
        )),
    CustomBooleanExpression(listOf())
)

private val numericExpressions = NumericExpressions(
    BaseNumericExpressions(
        listOf(
            concreteHOrNull(BooleanFunctionOutput.M) { (it.d + (it.d * it.e / 10)) },
            concreteHOrNull(BooleanFunctionOutput.P) { (it.d + (it.d * (it.e - it.f) / 25.5)).toFloat() },
            concreteHOrNull(BooleanFunctionOutput.T) { (it.d - (it.d * it.f / 30)) }
        )),
    CustomNumericExpressions(
        listOf(
            concreteHOrNull(BooleanFunctionOutput.P) { (2 * it.d + (it.d * it.e / 100)) }
        )
    ),
    CustomNumericExpressions(
        listOf(
            concreteHOrNull(BooleanFunctionOutput.M) { (it.f + it.d + (it.d * it.e / 100)) }
        )
    )
)

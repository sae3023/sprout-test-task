You are given a number of inputs and corresponding expressions for substitutions between inputs and outputs, and the expected output. 
Possible inputs 
Following variables are acceptable as input: 
A: bool 
B: bool 
C: bool 
D: float 
E: int 
F: int 
Expected outputs 

The outputs are defined as: 
H: one of these predefined values [M,P,T] (e.g. H = {M | P | T} 
K: floating point number (e.g. float, decimal) 

## Substitution expressions 
The assignment consists of base expressions set and two custom set of expressions that override / extend the base rules. 
Base 
A && B && !C => H = M 
A && B && C => H = P 
!A && B && C => H = T 
[other] => [error] 
H = M => K = D + (D * E / 10) 
H = P => K = D + (D * (E - F) / 25.5) 
H = T => K = D - (D * F / 30) 
Custom 1 
H = P => K = 2 * D + (D * E / 100) 
Custom 2 
A && B && !C => H = T 
A && !B && C => H = M 
H = M => K = F + D + (D * E / 100) 
Result 
Assignment could be implemented in a way of back-end application and be exposed in a simple REST API or as browser 
based application and run within the browser with or without communication to the backend. Shape of implementation is up to you. 
Applying 
The assignment should be sent either as a zip-folder or link to a Git repository. 
Please provide an accompanying description of architecture, implementation details why you decided to solve an assignment using a particular approach, framework, programming language, verification of code quality, etc. It’s an open format so feel free to express your decisions the way you feel makes more sense from your point of view. 
Please provide either in shape of README.md file or in any other format. 
